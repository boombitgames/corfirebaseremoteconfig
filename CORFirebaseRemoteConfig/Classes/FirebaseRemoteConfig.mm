#import "FirebaseRemoteConfig.h"
#import <CORCommon/CORCommon.h>

@implementation FirebaseRemoteConfig

BasicDelegate InitializeCallback;
StringDelegate ConfigFetchedCallback;
StringDelegate FetchFailedCallback;

int fetchInterval;
UIView *UnityGetGLView();

FIRRemoteConfig *remoteConfig;
bool whileFetchingFirebase = NO;

typedef void (^FIRRemoteConfigActivateCompletion)(NSError *_Nullable);

+ (FirebaseRemoteConfig*)sharedManager
{
    static FirebaseRemoteConfig *sharedSingleton;
    
    if( !sharedSingleton )
    {
        sharedSingleton = [[FirebaseRemoteConfig alloc] init];
    }
    
    return sharedSingleton;
}

-(void)InitializeConfig: (int) minimumFetchInterval
{
    NSLog(@"FirebaseRemoteConfig :: InitializeConfig :: minimumFetchInterval: %i", minimumFetchInterval);

    remoteConfig = [FIRRemoteConfig remoteConfig];
    FIRRemoteConfigSettings *remoteConfigSettings = [[FIRRemoteConfigSettings alloc] init];
    fetchInterval = minimumFetchInterval;
    remoteConfigSettings.minimumFetchInterval = fetchInterval;
    remoteConfig.configSettings = remoteConfigSettings;

    InitializeCallback();
}

-(void)FetchConfig{
    NSLog(@"FirebaseRemoteConfig :: FetchConfig");
    [self FetchConfig: fetchInterval];
}


-(void)FetchConfig: (int) expirationDuration
{
    NSLog(@"FirebaseRemoteConfig :: FetchConfig :: StartingFetch :: expirationDuration: %i", expirationDuration);
    
    if (whileFetchingFirebase) return;
    
    // [START fetch_config_with_callback]
    // TimeInterval is set to expirationDuration here, indicating the next fetch request will use
    // data fetched from the Remote Config service, rather than cached parameter values, if cached
    // parameter values are more than expirationDuration seconds old. See Best Practices in the
    // README for more information.
    
    // If your app is using developer mode, expirationDuration is set to 0, so each fetch will
    // retrieve values from the Remote Config service.
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"CONFIG_STALE"]) {
        expirationDuration = 0;
    }
    
    whileFetchingFirebase = YES;
    [remoteConfig fetchWithExpirationDuration:expirationDuration completionHandler:^(FIRRemoteConfigFetchStatus status, NSError *error) {
        if (status == FIRRemoteConfigFetchStatusSuccess) {
            
            NSLog(@"FirebaseRemoteConfig :: FetchConfig :: ParsingConfig :: expirationDuration: %i", expirationDuration);
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CONFIG_STALE"];
            
            [remoteConfig activateWithCompletion:^(BOOL changed, NSError * _Nullable error) {
                
                NSArray *array = [remoteConfig allKeysFromSource:FIRRemoteConfigSourceRemote];
                NSMutableDictionary *dict=[NSMutableDictionary new];
                
                for(int i = 0; i < array.count; i++)
                {
                    [dict setValue:remoteConfig[array[i]].stringValue forKey:array[i]];
                }
                
                if(array.count > 0)
                {
                    NSError* error = nil;
                    NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&error];
                    
                    if (error==nil)
                    {
                        NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                
                        ConfigFetchedCallback([json UTF8String]);
                        
                        NSLog(@"FirebaseRemoteConfig :: FetchConfig :: ConfigFetched :: Message: %s", [json UTF8String]);
                        
                    } else {
                        ConfigFetchedCallback("");
                        NSLog(@"FirebaseRemoteConfig :: FetchConfig :: ConfigFetchedEmpty :: Message: %s", "");
                    }
                } else {
                    ConfigFetchedCallback("");
                    NSLog(@"FirebaseRemoteConfig :: FetchConfig :: ConfigFetchedEmpty :: Message: %s", "");
                }
                
            }];
        } else {
            NSLog(@"FirebaseRemoteConfig :: FetchConfig :: ConfigNotFetched :: Error: %@", error.localizedDescription);
            FetchFailedCallback([error.localizedDescription UTF8String]);
        }
        whileFetchingFirebase = NO;
    }];
}

@end

extern "C" {
const void FirebaseInitializeConfig(int minimumFetchInterval) {
    [[FirebaseRemoteConfig sharedManager] InitializeConfig:minimumFetchInterval];
}
const void FirebaseFetchConfig() {
    [[FirebaseRemoteConfig sharedManager] FetchConfig];
}

void _RegisterNativeCallbacks(BasicDelegate _initializeCallback, StringDelegate _configFetchedCallback, StringDelegate _fetchFailedCallback){
    InitializeCallback = _initializeCallback;
    ConfigFetchedCallback = _configFetchedCallback;
    FetchFailedCallback = _fetchFailedCallback;
}

}
