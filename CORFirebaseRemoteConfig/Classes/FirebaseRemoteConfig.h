#import <UIKit/UIKit.h>
#import "Firebase.h"

#include <CORCommon/CORCommon.h>

@interface FirebaseRemoteConfig : NSObject

extern BasicDelegate InitializeCallback;
extern StringDelegate ConfigFetchedCallback;
extern StringDelegate FetchFailedCallback;

+ (FirebaseRemoteConfig*)sharedManager;
-(void)InitializeConfig: (int) minimumFetchInterval;
-(void)FetchConfig;

@end
