#
# Be sure to run `pod lib lint CORFirebaseRemoteConfig.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CORFirebaseRemoteConfig'
  s.version          = '1.3.9'
  s.summary          = 'Native components for Firebase Remote Config module.'
  s.homepage         = 'https://bitbucket.org/boombitgames/corfirebaseremoteconfig/src'
  s.license          = { :type => 'Proprietary software', :file => 'LICENSE' }
  s.author           = { 'CORE' => 'core@boombit.com' }
  s.source           = { :git => 'git@bitbucket.org:boombitgames/corfirebaseremoteconfig.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'
  s.static_framework = true
  s.source_files = 'CORFirebaseRemoteConfig/Classes/**/*'
  s.swift_version = '5.0'
  # s.resource_bundles = {
  #   'CORFirebaseRemoteConfig' => ['CORFirebaseRemoteConfig/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'

  s.dependency 'Firebase/RemoteConfig', '11.6.0'
  s.dependency 'Firebase/Analytics', '11.6.0'
  s.dependency 'CORCommon'

end
